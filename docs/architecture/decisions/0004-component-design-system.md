# 4. Component Design System

Date: 2021-04-02

## Status

Proposed

## Context

- Vue.js는 component base 구조로 디자인, publishing이 component 단위로 되어야 함
- component에 대한 의사소통, 명세서, 사용법에 대한 관리 필요
- component 단위로 테스트 가능해야 함
- Modern SPA framework 들의 component에 대한 디자인 시스템은 storybook을 일반적으로 쓰고 있음
- storybook은 figma와 zephlin에 대한 addon이 있고, 특히 figma는 Vue component로 바로 export할 수 있는 방법이 있음

## Decision
 - Storybook

## Consequences

- SPA 표준 코드 structure에 storybook을 설치한 상태로 시작할 수 있도록 한다.
- Component base publishing에 익숙치 않은 디자이너에게 사용법 교육한다.
- 디자이너가 storybook에 publishing하면 개발자가 그 component에 대한 action을 구현한다.
